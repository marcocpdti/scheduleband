export const colors = {
  blueActive: '#0077b6',

  textTitle: '#ced4da',
  textErrorInput: '#b23a48',
  placeholder: 'rgba(255, 255, 255, 0.8),',
  backgroundInput: 'rgba(0, 0, 0, 0.1)',
};
