import React from 'react';
import {View, Text, Modal} from 'react-native';

import {SModalView, SButton} from './styles';

const modalError: React.FC = (props) => {
  const {message, modalVisible, closeModal} = props;

  return (
    <View>
      <Modal
        animationType="fade"
        hardwareAccelerated
        transparent
        statusBarTranslucent
        visible={modalVisible}>
        <SModalView>
          <Text>{message}</Text>
          <SButton onPress={closeModal}>
            <Text>Tentar novamente</Text>
          </SButton>
        </SModalView>
      </Modal>
    </View>
  );
};

export default modalError;
