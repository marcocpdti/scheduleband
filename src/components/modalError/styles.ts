import styled from 'styled-components/native';
import {colors} from '../../styles/index';

export const SModalView = styled.View`
  margin: 60px;
  background-color: black;
  border-radius: 10px;
  padding: 35px;
  justify-content: center;
  align-items: center;
`;

export const SButton = styled.TouchableOpacity`
  height: 40px;
  border-radius: 8px;
  background-color: blue;
  width: 60%;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  background: ${colors.blueActive};
`;
