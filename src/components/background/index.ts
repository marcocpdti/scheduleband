import LinerGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

export default styled(LinerGradient).attrs({
  colors: ['#4449c1', '#ab59c1'],
})`
  flex: 1;
`;
