import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #233;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.Text`
  color: white;
`;

export const Input = styled.TextInput`
  background-color: #fff;
  width: 60%;
  border-radius: 6px;
  margin: 20px;
`;
