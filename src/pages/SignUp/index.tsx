import React from 'react';

import {useDispatch, useSelector} from 'react-redux';

import {Formik} from 'formik';
import {Button, TextInput, View, LogBox} from 'react-native';

import {Container, Title} from './styles';

const SignUp: React.FC = () => {
  LogBox.ignoreLogs(['Setting a timer']);
  async function createUser() {}

  return (
    <Container>
      <Title>Criar usuário</Title>
      <Formik
        initialValues={{username: '', password: ''}}
        onSubmit={(values) => {
          createUser;
        }}>
        {({handleChange, handleBlur, handleSubmit, values}) => (
          <View>
            <TextInput
              placeholder="Usuário"
              onChangeText={handleChange('username')}
              onBlur={handleBlur('username')}
              value={values.username}
            />
            <TextInput
              placeholder="Password"
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              value={values.password}
            />

            <Button onPress={createUser} title="Salvar usuário" />
          </View>
        )}
      </Formik>
    </Container>
  );
};

export default SignUp;
