import React from 'react';
import {Button} from 'react-native';

import {useDispatch} from 'react-redux';
import {logout} from '../../store/slices/userLogin/userLoginSlice';

import {Container, Title} from './styles';
import Background from '../../components/background';

const Home: React.FC = ({navigation}) => {
  const dispatch = useDispatch();

  const logoutSystem = () => {
    dispatch(logout());
    navigation.navigate('SignIn');
  };

  return (
    <Background>
      <Container>
        <Title>TELA HOME</Title>
        <Button onPress={logoutSystem} title="Sair" />
      </Container>
    </Background>
  );
};

export default Home;
