import styled from 'styled-components/native';
import {colors} from '../../styles/index';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.Text`
  color: ${colors.textTitle};
  font-size: 30px;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
`;

export const TInput = styled.TextInput.attrs({
  placeholderTextColor: colors.placeholder,
})`
  padding: 0 15px;
  height: 46px;
  background: ${colors.backgroundInput};
  font-size: 15px;
  width: 250px;
  margin: 10px 0;
  border-radius: 8px;
`;

export const Div = styled.View`
  margin-top: 20px;
`;

export const SImage = styled.Image`
  height: 100px;
  width: 170px;
  margin-bottom: 40px;
  opacity: 0.5;
`;

export const SButtonTouchable = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 50px;
  border-radius: 8px;
  background: ${colors.blueActive};
  margin-top: 20px;
`;

export const STextCreateAccount = styled.Text`
  font-size: 15px;
  text-align: right;
  margin-top: 15px;
`;

export const STextError = styled.Text`
  color: ${colors.textErrorInput};
`;
