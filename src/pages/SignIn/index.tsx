import React, {useState} from 'react';

import {useDispatch} from 'react-redux';

import {loginUser} from '../../store/slices/userLogin/userLoginSlice';

import {sessionUser} from '../../api';

import {Formik} from 'formik';
import {View, Text, StatusBar} from 'react-native';
import ModalError from '../../components/modalError';

import {
  Container,
  Title,
  TInput,
  SImage,
  SButtonTouchable,
  STextCreateAccount,
  STextError,
} from './styles';
import Background from '../../components/background';

import * as yup from 'yup';

// App.js

const loginValidationSchema = yup.object().shape({
  username: yup
    .string()
    .email('Informe um email válido')
    .required('Informe um email'),
  password: yup
    .string()
    .min(6, ({min}) => `A senha deve ter no mínimo ${min} caracteres`)
    .required('Informe uma senha'),
});

const SignIn: React.FC = ({navigation}) => {
  const dispatch = useDispatch();

  const [modalStatus, setModalStatus] = useState(false);

  const submit = async ({username = '', password = ''}) => {
    const body = {user_email: username, password: password};
    try {
      let response = await sessionUser('sessions', body);

      if (response.data) {
        dispatch(loginUser(response.data));
        navigation.navigate('Home');
      }
      if (response.status === 401) {
        setModalStatus(true);
      }
    } catch (error) {
      console.log('xiiiiiiiiiii => ', error);
    }
  };

  const toCloseModal = () => {
    setModalStatus(false);
  };

  const redirectPage = () => {
    navigation.navigate('SignUp');
  };

  return (
    <Background>
      <Container>
        <StatusBar translucent backgroundColor="transparent" />
        <SImage source={require('../../assets/images/logo.png')} />
        <Title>Show Bands - Agenda</Title>
        {modalStatus && (
          <ModalError
            message="Usuário ou senha incorretos"
            modalVisible={modalStatus}
            closeModal={toCloseModal}
          />
        )}
        {!modalStatus && (
          <Formik
            validationSchema={loginValidationSchema}
            initialValues={{username: '', password: ''}}
            onSubmit={(values) => {
              submit(values);
            }}>
            {({handleChange, handleBlur, handleSubmit, values, errors}) => (
              <View>
                <TInput
                  placeholder="Usuário"
                  onChangeText={handleChange('username')}
                  onBlur={handleBlur('username')}
                  value={values.username}
                  keyboardType="email-address"
                />
                {errors.username && <STextError>{errors.username}</STextError>}
                <TInput
                  placeholder="Password"
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  secureTextEntry
                />
                {errors.password && <STextError>{errors.password}</STextError>}
                <View>
                  <SButtonTouchable onPress={handleSubmit}>
                    <Text>Acessar agenda</Text>
                  </SButtonTouchable>
                </View>
                <View>
                  <STextCreateAccount onPress={redirectPage}>
                    Criar conta
                  </STextCreateAccount>
                </View>
              </View>
            )}
          </Formik>
        )}
      </Container>
    </Background>
  );
};

export default SignIn;
