/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import * as React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Home from './pages/Home';
import SignIn from './pages/SignIn';
import Users from './pages/Users';
import SignUp from './pages/SignUp';

import store from './store';

const App = () => {
  const Drawer = createDrawerNavigator();

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Drawer.Navigator initialRouteName="SignIn">
          <Drawer.Screen name="Home" component={Home} />
          <Drawer.Screen name="SignIn" component={SignIn} />
          <Drawer.Screen name="Users" component={Users} />
          <Drawer.Screen name="SignUp" component={SignUp} />
        </Drawer.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
